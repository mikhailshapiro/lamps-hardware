#include <EEPROM.h>

#include <Arduino.h>

#include <Rotary.h>

//Wifi/WebSocket
#include <ESP8266WiFi.h>

//#include <ESP8266WiFiMulti.h>
#include <SocketIoClient.h>
#define USE_SERIAL Serial

//wifi manager
#include <DNSServer.h>            //Local DNS Server used for redirecting all requests to the configuration portal
#include <ESP8266WebServer.h>     //Local WebServer used to serve the configuration portal
#include <WiFiManager.h>          //https://github.com/tzapu/WiFiManager WiFi Configuration Magic

//ArduinoJSON
#include <ArduinoJson.h>

//Onewire
#include <OneWire.h>
OneWire ds(0);               

//Neopixels
#include <Adafruit_NeoPixel.h>
#define PIN 15
Adafruit_NeoPixel strip = Adafruit_NeoPixel(1, PIN, NEO_RGB + NEO_KHZ800);
uint8_t brightness = 200;
uint8_t maxBrightness = 255;
Rotary rotary = Rotary(4, 5);
Rotary rotaryBrightness = Rotary(2, 14);
boolean displayRainbow = false;
boolean displayRandom = false;
boolean emitColor = false;
boolean emitRainbowFromDevice = false;
uint8_t currentColor = 0;

String serialNumber;

unsigned long emitTime;

unsigned long actionTime = 0;

//ESP8266WiFiMulti WiFiMulti;
SocketIoClient webSocket;

void rainbowEvent(const char * payload, size_t length) {
  displayRainbow = true;

  webSocket.emit("plainString", "\"this is a plain string\"");
}

void connectEvent(const char * payload, size_t length){
  Serial.println("connected to websocket");
}

void rgbEvent(const char * payload, size_t length){
    USE_SERIAL.printf("got message: %s\n", payload);
    StaticJsonBuffer<200> jsonBuffer;
    JsonObject& root = jsonBuffer.parseObject(payload);
    if (!root.success()) {
      Serial.println("parseObject() failed");
      return;
    }
    uint8_t target = root["newColor"];
    partialRainbow(20,currentColor, target);
    currentColor = target;
    //currentColor = String(payload).toInt();
}

void setup() {
  
    strip.begin();
    strip.setBrightness(0);

    strip.show(); // Initialize all pixels to 'off'
    
    USE_SERIAL.begin(115200);

    /*EEPROM.begin(512);
    delay(500);
    USE_SERIAL.setDebugOutput(true);
    EEPROM.get(0, serialNumber);
    delay(500);
    Serial.println("serial number from address: "+String(serialNumber));
    Serial.println(serialNumber == 255);
    if(serialNumber == 255){
      Serial.println("EEPROM if");
      serialNumber = random(0,100);
      delay(500);
      EEPROM.put(0, serialNumber); //assign serial number
      //EEPROM.commit();
      delay(500);
      EEPROM.get(0, serialNumber);
      delay(500);
      EEPROM.commit();
    }
    /*EEPROM.put(0,255);
    delay(500);*/
    /*EEPROM.commit();
    delay(500);*/

    
    serialNumber = getSerialNumber();

    
    Serial.println("Serial Number 2 is: "+serialNumber);

    pinMode(13, INPUT_PULLUP);
    pinMode(12, INPUT_PULLUP);
    pinMode(4, INPUT_PULLUP);
    pinMode(5, INPUT_PULLUP);
    pinMode(14, INPUT_PULLUP);
    pinMode(2, INPUT_PULLUP);

    
    USE_SERIAL.println();
    USE_SERIAL.println();
    USE_SERIAL.println();

      for(uint8_t t = 1; t > 0; t--) {
          USE_SERIAL.printf("[SETUP] BOOT WAIT %d...\n", t);
          USE_SERIAL.flush();
          delay(1000);
      }

    /*WiFiMulti.addAP("clarence", "aquasnacks");

    while(WiFiMulti.run() != WL_CONNECTED) {
        delay(500);
    }
    */

    WiFiManager wifiManager;
  
    Serial.begin(115200);
    delay(100);
    Serial.println(digitalRead(12));
    if(!digitalRead(12)){
      wifiManager.resetSettings();
    }

    attachInterrupt(digitalPinToInterrupt(13), emitRainbow, FALLING); 
    attachInterrupt(digitalPinToInterrupt(12), randomColor, FALLING); 
    attachInterrupt(digitalPinToInterrupt(4), rotate , CHANGE); 
    attachInterrupt(digitalPinToInterrupt(5), rotate, CHANGE); 
    attachInterrupt(digitalPinToInterrupt(14), changeBrightness, CHANGE); 
    attachInterrupt(digitalPinToInterrupt(2), changeBrightness, CHANGE); 

    String networkNameStr = String("Lights-")+String(serialNumber);
    const char* networkName = networkNameStr.c_str();
    wifiManager.autoConnect(networkName);
    
    // We start by connecting to a WiFi network
   

    webSocket.on("rainbow", rainbowEvent);
    webSocket.on("connect", connectEvent);
    webSocket.on("rgb", rgbEvent);
    webSocket.on("sendSerialNumber", sendSerialNumberEvent);

    //webSocket.begin("192.168.1.60",8080); //local
    webSocket.begin("extended-inn-227105.appspot.com",80); //cloud
    // use HTTP Basic Authorization this is optional remove if not needed
    //webSocket.setAuthorization("username", "password");
    delay(400);
    sendSerialNumber();
}

void loop() {
  webSocket.loop();
  
  if(displayRainbow){
    rainbow(20, currentColor);
    displayRainbow = false;
  }
  if(displayRandom){
    currentColor = random(0,255);
    actionTime = millis();
    emitColor = true;
    displayRandom = false;
  }
  
  //strip.setBrightness(brightness);
  strip.setBrightness(brightness);
  strip.setPixelColor(0, Wheel(currentColor));
  strip.show();

  if(emitColor){
   emitTime = millis();
   if((emitTime - actionTime) > 2000){
      //serializeColor();
      String rgbConcat = 
        String("{")+
          String("\"serialNumber\":\"")+String(serialNumber)+
          String("\",\"newColor\":\"")+String(currentColor)+
        String("\"}");
      const char* rgbSend = rgbConcat.c_str();
      webSocket.emit("rgb", rgbSend);
      actionTime = millis();
      emitColor = false;
    }
  }

  if(emitRainbowFromDevice){
   emitTime = millis();
   if((emitTime - actionTime) > 2000){
      //serializeColor();
      String rainConcat = 
      String("{")+
          String("\"serialNumber\":\"")+String(serialNumber)+
      String("\"}");
      const char* rainSend = rainConcat.c_str();
      webSocket.emit("rainbow", rainSend);
      actionTime = millis();
      emitRainbowFromDevice = false;
    }
  }
}



void rainbow(uint8_t wait, uint16_t current) {
  actionTime = millis();
  uint16_t j;
    for(j=current; j<(current+256); j++) {
      currentColor = (j) & 255;
      strip.setBrightness(brightness);
      strip.setPixelColor(0, Wheel(currentColor));
      strip.show();
      delay(wait);
    }
}

/*void partialRainbow(uint8_t wait, uint8_t current, uint8_t target){
    uint8_t j;
    boolean isForwards = target > current;
    uint16_t forwardsDistance = abs((target + 255) - (current + 255));
    uint16_t backwardsDistance = abs((current + 255) - (target + 255));
    Serial.println("forwards: "+String(forwardsDistance)+" backwards:"+String(backwardsDistance));
    Serial.print(isForwards);
    for(j=current; j!=target; j++) {
      currentColor = (j) & 255;
      strip.setBrightness(brightness);
      strip.setPixelColor(0, Wheel(currentColor));
      strip.show();
      delay(wait);
    }
}*/

void partialRainbow(uint8_t wait, uint8_t current, uint8_t target){
    uint8_t j;
    boolean isForwards = target > current;
    uint16_t forwardsDistance = abs((target + 255) - (current + 255));
    uint16_t backwardsDistance = abs((current + 255) - (target + 255));
    Serial.println("forwards: "+String(forwardsDistance)+" backwards:"+String(backwardsDistance));
    Serial.print(isForwards);
    int escape = 0;
    while((Wheel(current) != Wheel(target)) || (escape == 255)) {
      current++;
      currentColor = (current) & 255;
      strip.setBrightness(brightness);
      strip.setPixelColor(0, Wheel(currentColor));
      strip.show();
      delay(wait);
    }
}


// Input a value 0 to 255 to get a color value.
// The colours are a transition r - g - b - back to r.
uint32_t Wheel(byte WheelPos) {
  WheelPos = 255 - WheelPos;
  if(WheelPos < 85) {
    return strip.Color(255 - WheelPos * 3, 0, WheelPos * 3);
  }
  if(WheelPos < 170) {
    WheelPos -= 85;
    return strip.Color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
  WheelPos -= 170;
  return strip.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
}


void emitRainbow(){
  Serial.println("emit rainbow");
  displayRainbow = true;
  emitRainbowFromDevice = true;
}

void randomColor(){
  Serial.println("random color");
  displayRandom = true;
}

// rotate is called anytime the rotary inputs change state.
void rotate() {
  unsigned char result = rotary.process();
  Serial.println("rotate");
  if (result == DIR_CW) {
    currentColor=currentColor+2;
    Serial.println(currentColor);
    emitColor = true;
  } else if (result == DIR_CCW) {
    currentColor=currentColor-2;
    Serial.println(currentColor);
    emitColor = true;
  }
}

void changeBrightness() {
  Serial.println("changeBrightness");
  Serial.println(brightness);
  unsigned char result = rotaryBrightness.process();
  if (result == DIR_CW) {
    if(brightness < maxBrightness){
      brightness= brightness+3;
    }
    Serial.println(brightness);
  } else if (result == DIR_CCW) {
    if(brightness > 0){
      brightness=brightness-3;
    }
    Serial.println(brightness);
  }

}

String getSerialNumber(){
  String serialNum = "";
  byte i;           // This is for the for loops
  boolean present;  // device present varj
  byte data[8];     // container for the data from device
  byte crc_calc;    //calculated CRC
  byte crc_byte;    //actual CRC as sent by DS2401
  //1-Wire bus reset, needed to start operation on the bus,
  //returns a 1/TRUE if presence pulse detected
  present = ds.reset();
  if (present)
  {
    Serial.println("---------- Device present ----------");
    ds.write(0x33);  //Send Read data command
    data[0] = ds.read();
    Serial.print("Family code: 0x");
    PrintTwoDigitHex (data[0], 1);
    Serial.print("Hex ROM data: ");
    for (i = 1; i <= 6; i++)
    {
      data[i] = ds.read(); //store each byte in different position in array
      serialNum += data[i];
      PrintTwoDigitHex (data[i], 0);
      Serial.print(" ");
    }
    Serial.println();
    crc_byte = ds.read(); //read CRC, this is the last byte
    crc_calc = OneWire::crc8(data, 7); //calculate CRC of the data
    Serial.print("Calculated CRC: 0x");
    PrintTwoDigitHex (crc_calc, 1);
    Serial.print("Actual CRC: 0x");
    PrintTwoDigitHex (crc_byte, 1);
    Serial.println(serialNum);
  }
  else  {
    Serial.println("xxxxx Nothing connected xxxxx");
    serialNum = 2;
  }
  return serialNum;
}
void PrintTwoDigitHex (byte b, boolean newline)
{
  Serial.print(b/16, HEX);
  Serial.print(b%16, HEX);
  if (newline) Serial.println();
}

void sendSerialNumberEvent(const char * payload, size_t length){
  sendSerialNumber();
}

void sendSerialNumber(){
      String hiConcat = 
        String("{") +
          String("\"serialNumber\":\"")+String(serialNumber) +
        String("\"}");
    const char* hiSend = hiConcat.c_str();
    webSocket.emit("deviceInfo",hiSend);
}
